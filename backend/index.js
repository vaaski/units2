console.log("\x1Bc") // clear console

const io = require("socket.io")(),
  rp = require("request-promise"),
  jsdom = require("jsdom"),
  { JSDOM } = jsdom,
  virtualConsole = new jsdom.VirtualConsole(),
  throttle = require("lodash/throttle"),
  storage = require("node-persist")

virtualConsole.on("error", e => console.log(`error: ${e}`))
virtualConsole.on("warn", e => console.log(`warn: ${e}`))
virtualConsole.on("info", e => console.log(`info: ${e}`))
virtualConsole.on("dir", e => console.log(`dir: ${e}`))
process.on("uncaughtException", err => console.log(`caught exception: ${err}`))

const url = ({ usr, pwd }) =>
  // `https://${usr}:${pwd}@osz2.barnim.de/fileadmin/user_upload/gpuntis/abteilung4/klassen`
  `https://${usr}:${pwd}@osz2-stundenplan.de/stundenplan/abteilung2/klassen2`

console.log("start @:420")
io.listen(420)

const students = {
  list: {},
  time: 0,
}

const savestuds = throttle(() => {
  console.log("saving students")
  students.time = +new Date()
  storage.setItem("students", students)
}, 2e3)

storage.init().then(() =>
  storage.getItem("students").then(db => {
    if (!db) return
    students.list = db.list
    students.time = db.time
  })
)

const n2str = nr => {
  let str = nr.toString()
  while (str.length < 5) str = "0" + str
  return str
}

Date.prototype.getWeek = function() {
  const target = new Date(this.valueOf())
  const dayNr = (this.getDay() + 6) % 7
  target.setDate(target.getDate() - dayNr + 3)
  const firstThursday = target.valueOf()
  target.setMonth(0, 1)
  if (target.getDay() != 4)
    target.setMonth(0, 1 + ((4 - target.getDay() + 7) % 7))
  const ret = 1 + Math.ceil((firstThursday - target) / 604800000)
  if (String(ret).length < 2) return "0" + ret
  else return ret
}

const getFullStudents = ({ usr, pwd }) => {
  if (students.time > Date.now() - 1e3 * 60 * 60 * 2) return

  rp({
    uri: `${url({ usr, pwd })}/frames/navbar.htm`,
    transform: body =>
      new JSDOM(body, { runScripts: "dangerously", virtualConsole }),
  })
    .then(({ window: w }) => {
      w.students.forEach((surname, key) =>
        students.list[key + 1]
          ? Object.assign(students.list[key + 1], { surname })
          : (students.list[key + 1] = { surname })
      )

      console.log("getting all students")
      for (const person in students.list)
        if (students.list.hasOwnProperty(person)) {
          const stud = students.list[person]
          rp({
            uri: `${url({ usr, pwd })}/${new Date().getWeek()}/s/s${n2str(
              person
            )}.htm`,
            transform: body => new JSDOM(body),
          }).then(({ window }) => {
            Object.assign(stud, {
              fullName: window.document
                .querySelector("center>font[size='4']")
                .innerHTML.replace(/\n/g, ""),
              // time: +new Date(),
            })
            savestuds()
          })
        }
    })
    .catch(error => console.log(error))
}

io.on("connection", socket => {
  socket.on("verifyCreds", ({ usr, pwd }, reply) => {
    rp(`${url({ usr, pwd })}/default.htm`)
      .then(() => reply(true))
      .catch(() => reply(false))
  })
  socket.on("getStudents", ({ usr, pwd }, reply) => {
    const copy = Object.assign([], students.list)
    copy.forEach(s => {
      // delete s.time
      delete s.surname
    })
    reply(Object.assign({}, copy))
    getFullStudents({ usr, pwd })
  })
  socket.on("getPlan", ({ usr, pwd, id, week }, reply) => {
    rp({
      uri: `${url({ usr, pwd })}/${week}/s/s${n2str(id)}.htm`,
      transform: body => new JSDOM(body),
    })
      .then(({ window }) => {
        const { document } = window
        document.querySelectorAll("table font[color]").forEach(element => {
          if (element.getAttribute("color") === "#FF0000")
            element.setAttribute("class", "changed")
          element.removeAttribute("color")
        })
        document
          .querySelectorAll("[face]")
          .forEach(e => e.removeAttribute("face"))

        const name = document
          .querySelector("center>font[size='4']")
          .innerHTML.replace(/\n/g, "")

        console.log(
          `plan[${new Date().toLocaleTimeString()}] ${name} for week ${week}`
        )

        reply({
          date: document
            .querySelector("center>font[size='3']:last-of-type")
            .innerHTML.replace(/(\n| {2,})/g, ""),
          name,
          table: document.querySelector("table").innerHTML,
          time: +new Date(),
        })
      })
      .catch(error => reply(error))
  })
})
