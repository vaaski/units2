import Vue from "vue"
import app from "./app.vue"
import router from "./router"
import store from "./store"
import io from "socket.io-client"
import "./registerServiceWorker"
import "vue-material/dist/vue-material.min.css"
import Raven from "raven-js"
import RavenVue from "raven-js/plugins/vue"
import "./assets/fonts.styl"
import "./assets/theme.scss"

Raven.config("https://a2bd0c196ede4f19b3281ca7e31651f4@sentry.io/1358112")
  .addPlugin(RavenVue, Vue)
  .install()

Vue.raven = Vue.prototype.raven = Raven
Vue.socket = Vue.prototype.socket =
  location.hostname === "localhost"
  // ? io("http://192.169.69.69:420")
    ? io("http://localhost:420")
    : io("https://colo.vaaski.com", { path: "/units2" })

Vue.config.productionTip = false

Date.prototype.getWeek = function() {
  const target = new Date(this.valueOf())
  const dayNr = (this.getDay() + 6) % 7
  target.setDate(target.getDate() - dayNr + 3)
  const firstThursday = target.valueOf()
  target.setMonth(0, 1)
  if (target.getDay() != 4)
    target.setMonth(0, 1 + ((4 - target.getDay() + 7) % 7))
  const ret = 1 + Math.ceil((firstThursday - target) / 604800000)
  if (String(ret).length < 2) return "0" + ret
  else return ret
}

const imp = ["Field", "Button", "Snackbar", "EmptyState"].forEach(e =>
  Vue.use(require("vue-material/dist/components")["Md" + e])
)

new Vue({
  router,
  store,
  render: h => h(app),
}).$mount("#app")
