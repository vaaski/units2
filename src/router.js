import Vue from "vue"
import Router from "vue-router"

Vue.use(Router)

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      alias: "/login",
      name: "login",
      component: require("./views/login.vue").default,
    },
    {
      path: "/studselect",
      name: "studselect",
      component: require("./views/studselect.vue").default,
    },
    {
      path: "/plan/:id/:week?",
      name: "plan",
      component: require("./views/plan.vue").default,
    },
    {
      path: "*",
      redirect: "/",
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ],
})
