import Vue from "vue"
import Vuex from "vuex"
import ls from "local-storage"

Vue.use(Vuex)

const def = {
  crd: false,
  students: {},
  cached_plans: {},
  online: true,
  lastid: false,
}

export default new Vuex.Store({
  state: ls("units2-store")
    ? Object.assign(def, ls("units2-store"), { online: navigator.onLine })
    : def,
  mutations: {
    store: (state, { change, value }) => {
      state[change] = value
      ls("units2-store", state)
    },
  },
  actions: {},
})
